import re
import xml.sax

class XmlWalker:
    class Node:
        def __init__(self, walker, idx):
            self.walker = walker
            self.meta = walker.walkdef.nodes[idx]

    class NopNode(Node):
        def begin(self, tag, attrs):
            pass

        def text(self, text):
            pass

        def end(self, tag):
            pass

    class StaticNode(Node):
        def begin(self, tag, attrs):
            cls = self.meta.handler
            fn = getattr(cls, "begin", None)
            if callable(fn):
                fn(self.walker.ctx, tag, attrs)

        def text(self, text):
            cls = self.meta.handler
            fn = getattr(cls, "text", None)
            if callable(fn):
                fn(self.walker.ctx, text)

        def end(self, tag):
            cls = self.meta.handler
            fn = getattr(cls, "end", None)
            if callable(fn):
                fn(self.walker.ctx, tag)

    class InstanceNode(Node):
        def __init__(self, walker, idx):
            super().__init__(walker, idx)
            self.stack = []

        def begin(self, tag, attrs):
            instance = self.meta.handler(self.walker.ctx, tag, attrs)
            self.stack.append(instance)

        def text(self, text):
            instance = self.stack[-1]
            fn = getattr(instance, "text", None)
            if callable(fn):
                fn(self.walker.ctx, text)

        def end(self, tag):
            instance = self.stack.pop()
            fn = getattr(instance, "end", None)
            if callable(fn):
                fn(self.walker.ctx, tag)


    class MetaNode:
        def __init__(self):
            self.name = None
            self.handler = None

    class Context:
        def __init__(self, walker):
            self.walker = walker

        # Get a handler instance from a node stack.
        # 'handler' is a node handler class.
        def object(self, handler, idx = -1):
            byhandler = self.walker.walkdef.byhandler
            if not handler in byhandler:
                return None
            node = self.walker.nodes[byhandler[handler]]
            if not isinstance(node, XmlWalker.InstanceNode):
                raise TypeError("xml walker context method 'object()' can only be called on node classes that are instantiated")
            return node.stack[idx]

        # Get stack depth.
        # If 'handler' is None, returns total depth.
        # If 'handler' is a node handler class, return the stack depth at that node.
        def depth(self, handler = None):
            if handler:
                node = self.walker.nodes[byhandler[handler]]
                if not isinstance(node, XmlWalker.InstanceNode):
                    raise TypeError("xml walker context method 'object()' can only be called on node classes that are instantiated")
                return len(node.stack)
            else:
                return len(self.walker.stack)

        # Like print(), but indents by self.depth()
        def print(self, *args, **kwargs):
            indent = (self.depth() - 1) * "  "
            print(indent, end = "")
            print(*args, **kwargs)


    class Sax(xml.sax.handler.ContentHandler):
        def __init__(self, walkdef, context):
            self.walkdef = walkdef
            self.ctx = context(self)
            self.nodes = [ None ] * len(walkdef.nodes)
            self.cursor = 0
            self.ignore = 0
            self.stack = []
            for i in range(len(walkdef.nodes)):
                meta = walkdef.nodes[i]
                if meta.handler:
                    fn = getattr(meta.handler, "__init__", None)
                    if fn == object.__init__:
                        node = XmlWalker.StaticNode(self, i)
                    else:
                        node = XmlWalker.InstanceNode(self, i)
                else:
                    node = XmlWalker.NopNode(self, i)
                self.nodes[i] = node

            for name in walkdef.byname:
                idx = walkdef.byname[name]
                self.nodes[idx].name = name

        def startElement(self, tag, attrs_overengineered):
            if self.ignore > 0:
                self.ignore += 1
                return

            attrs = {}
            for k, v in attrs_overengineered.items():
                attrs[k] = v

            target = None;
            if (self.cursor, tag) in self.walkdef.descend: # named edge
                key = (self.cursor, tag)
                target = self.walkdef.descend[key]
            elif self.cursor in self.walkdef.descend: # wildcard/function edge
                key = self.cursor
                funcs = self.walkdef.descend[key]
                for fn in funcs:
                    if fn[0] == None:
                        target = fn[1]
                    elif fn[0](tag):
                        target = fn[1]
                        break
                
            if not target:
                self.ignore = 1
                return

            self.stack.append(self.cursor)
            self.cursor = target
            node = self.nodes[self.cursor]

            node.begin(tag, attrs)
            
        def endElement(self, tag):
            if self.ignore > 0:
                self.ignore -= 1
                return
            node = self.nodes[self.cursor]
            node.end(tag)
            self.cursor = self.stack.pop()

        def characters(self, text):
            if self.ignore > 0:
                return
            node = self.nodes[self.cursor]
            node.text(text)

    def __init__(self, nnode, handlers, handlersPrefix):
        self.nodes = [ None ] * nnode
        for i in range(nnode):
            self.nodes[i] = XmlWalker.MetaNode()
        self.byhandler = {}
        for name in self.byname:
            clsname = handlersPrefix + name
            if not clsname in handlers:
                continue
            handler = handlers[clsname]
            if not isinstance(handler, type):
                continue
            idx = self.byname[name]
            self.nodes[idx].handler = handler
            self.byhandler[handler] = idx

    def saxwalk(self, src, context = Context):
        sax = XmlWalker.Sax(self, context)
        parser = xml.sax.make_parser()
        parser.setContentHandler(sax)
        parser.parse(src)
        return sax.ctx

    def saxwalkString(self, src, context = Context):
        sax = XmlWalker.Sax(self, context)
        parser = xml.sax.make_parser()
        parser.setContentHandler(sax)
        parser.parseString(src)
        return sax.ctx

class GraphmlWalker(XmlWalker):
	def __init__(self, handlers, handlersPrefix = ""):
		self.byname = {
			'Arrows': 9,
			'Col': 10,
			'ColLabelParam': 11,
			'Cols': 12,
			'Document': 13,
			'Edge': 14,
			'EdgeData': 15,
			'EdgeLabel': 16,
			'EdgeStyle': 17,
			'GenericEdge': 18,
			'GenericNode': 19,
			'Graph': 20,
			'Insets': 21,
			'Key': 22,
			'LineStyle': 23,
			'NestedError': 24,
			'Node': 25,
			'NodeAttributeLabel': 26,
			'NodeData': 27,
			'NodeGeometry': 28,
			'NodeLabel': 29,
			'NodeMethodLabel': 30,
			'NodeStyle': 31,
			'Row': 32,
			'RowLabelParam': 33,
			'Rows': 34,
			'Shape': 35,
			'Table': 36,
			'TableNode': 37,
			'UmlClassNode': 38,
			'UmlNode': 39,
			'UmlNoteNode': 40,
		}
		self.descend = {
			(0, 'graphml'): 13,
			(1, 'y:Arrows'): 9,
			(1, 'y:EdgeLabel'): 16,
			(1, 'y:LineStyle'): 23,
			(1, 'y:StyleProperties'): 2,
			(2, 'y:Property'): 17,
			(3, 'y:Property'): 31,
			(4, 'y:ColumnNodeLabelModelParameter'): 11,
			(4, 'y:RowNodeLabelModelParameter'): 33,
			(5, 'y:Geometry'): 28,
			(5, 'y:NodeLabel'): 29,
			(5, 'y:StyleProperties'): 3,
			(6, 'y:GenericGroupNode'): 19,
			(6, 'y:GroupNode'): 5,
			(7, 'y:Realizers'): 6,
			(8, 'y:Geometry'): 28,
			(8, 'y:NodeLabel'): 29,
			(8, 'y:Shape'): 35,
			(8, 'y:StyleProperties'): 3,
			(10, 'y:Columns'): 24,
			(10, 'y:Insets'): 21,
			(12, 'y:Column'): 10,
			(13, 'graph'): 20,
			(13, 'key'): 22,
			(14, 'data'): 15,
			(15, 'y:GenericEdge'): 18,
			(15, 'y:PolyLineEdge'): 1,
			(18, 'y:Arrows'): 9,
			(18, 'y:EdgeLabel'): 16,
			(18, 'y:LineStyle'): 23,
			(18, 'y:StyleProperties'): 2,
			(19, 'y:Geometry'): 28,
			(19, 'y:NodeLabel'): 29,
			(19, 'y:StyleProperties'): 3,
			(20, 'edge'): 14,
			(20, 'node'): 25,
			(25, 'data'): 27,
			(25, 'graph'): 20,
			(27, 'y:GenericNode'): 19,
			(27, 'y:ProxyAutoBoundsNode'): 7,
			(27, 'y:ShapeNode'): 8,
			(27, 'y:TableNode'): 37,
			(27, 'y:UMLClassNode'): 38,
			(27, 'y:UMLNoteNode'): 40,
			(29, 'y:ModelParameter'): 4,
			(32, 'y:Insets'): 21,
			(32, 'y:Rows'): 24,
			(34, 'y:Row'): 32,
			(36, 'y:Columns'): 12,
			(36, 'y:Insets'): 21,
			(36, 'y:Rows'): 34,
			(37, 'y:Geometry'): 28,
			(37, 'y:NodeLabel'): 29,
			(37, 'y:StyleProperties'): 3,
			(37, 'y:Table'): 36,
			(38, 'y:Geometry'): 28,
			(38, 'y:NodeLabel'): 29,
			(38, 'y:StyleProperties'): 3,
			(38, 'y:UML'): 39,
			(39, 'y:AttributeLabel'): 26,
			(39, 'y:MethodLabel'): 30,
			(40, 'y:Geometry'): 28,
			(40, 'y:NodeLabel'): 29,
			(40, 'y:StyleProperties'): 3,
		}
		super().__init__(41, handlers, handlersPrefix)

def make_walker_from_graphml(filename):
    class Node:
        def __init__(self, ctx, tag, attrs):
            self.mlid = attrs["id"]
            self.id = None
            self.label = None
            self.isComment = False
            self.x = 0.0
            self.y = 0.0
            self.incoming = []
            self.outgoing = []
            self.isEntry = False
            self.name = None
            ctx.nodes[self.mlid] = self

        def finish(self):
            if self.label:
                cap = NodeLabel.regex.match(self.label)
                if not cap:
                    raise ValueError("Invalid edge label on edge " + self.mlid + ": '" + self.label + "'")
                elif cap.group("entry"):
                    self.isEntry = True
                else:
                    self.name = cap.group("name")

        def key(self):
            name = self.name
            if not name:
                name = ""
            return (not self.isEntry, name, self.mlid)

        def __repr__(self):
            s = "{ 'x': " + str(self.x) + ", 'y': " + str(self.y)
            if self.isComment:
                s += ", 'isComment': True"
            if self.isEntry:
                s += ", 'isEntry': True"
            if self.label:
                s += ", 'label': '" + str(self.label) + "'"
            s += " }"
            return s

    class NodeLabel:
        regex = re.compile(r"^\s*((?P<entry>/)|(?P<name>[a-zA-Z_][a-zA-Z0-9_]*))?\s*$")

        def text(ctx, text):
            text = text.strip()
            if len(text) == 0:
                return
            node = ctx.object(Node)
            if node.label:
                node.label += text
            else:
                node.label = text

    class NodeStyle:
        def begin(ctx, tag, attrs):
            if "value" in attrs and attrs["value"] == "ARTIFACT_TYPE_ANNOTATION":
                node = ctx.object(Node)
                node.isComment = True

    class GenericNode:
        def begin(ctx, tag, attrs):
            if "configuration" in attrs and attrs["configuration"] == "com.yworks.flowchart.annotation":
                node = ctx.object(Node)
                node.isComment = True

    class UmlNoteNode:
        def begin(ctx, tag, attrs):
            node = ctx.object(Node)
            node.isComment = True

    class NodeGeometry:
        def begin(ctx, tag, attrs):
            node = ctx.object(Node)
            if "x" in attrs:
                node.x = float(attrs["x"])
            if "y" in attrs:
                node.y = float(attrs["y"])

    class Edge:
        def __init__(self, ctx, tag, attrs):
            self.mlid = attrs["id"]
            self.source = attrs["source"]
            self.target = attrs["target"]
            self.sarrow = None
            self.tarrow = None
            self.label = None
            self.snode = None
            self.tnode = None
            self.tag = None
            self.priority = 0
            ctx.edges.append(self)

        def finish(self):
            # ensure visual arrows match functional representation
            if self.sarrow == "none" and self.tarrow == "standard":
                pass
            elif self.sarrow == "standard" and self.tarrow == "none":
                self.source, self.target = self.target, self.source
                self.sarrow, self.tarrow = self.tarrow, self.sarrow
                self.snode, self.tnode = self.tnode, self.snode
            else:
                raise ValueError("Unrecognized arrows on edge " + self.mlid)

            self.snode.outgoing.append(self)
            self.tnode.incoming.append(self)

            # parse labels
            if self.label:
                cap = EdgeLabel.regex.match(self.label)
                if not cap:
                    raise ValueError("Invalid edge label on edge " + self.mlid + ": '" + self.label + "'")
                if cap.group("tgtname"): # use target node name as tag
                    self.tag = self.tnode.name
                if cap.group("wildcard"):
                    self.tag = "*"
                else:
                    self.tag = cap.group("tag")
                priority = cap.group("priority")
                if priority:
                    self.priority = int(priority)

        def key(self):
            assert self.tag
            return (self.snode.id, self.tag)

        def __repr__(self):
            s = "{ 'source': '" + self.source + "', 'target': " + self.target
            if self.label:
                s += ", 'label': '" + str(self.label) + "'"
            if self.sarrow:
                s += ", 'sarrow': '" + str(self.sarrow) + "'"
            if self.tarrow:
                s += ", 'tarrow': '" + str(self.tarrow) + "'"
            s += " }"
            return s

        def make_expand(a, b):
            edge = Edge.__new__(Edge)
            edge.snode = a.snode
            edge.tnode = b.tnode
            edge.source = a.source
            edge.target = b.target
            edge.priority = a.priority + b.priority
            edge.tag = b.tag
            edge.label = "<expanded>"
            edge.sarrow = "none"
            edge.tarrow = "standard"
            return edge


    class EdgeLabel:
        regex = re.compile(r"^\s*(\[(?P<priority>[+-]?[0-9]+)\])?\s*((?P<tgtname>[<][>])|(?P<wildcard>[*])|(?P<tag>[^\s]+))?\s*$")

        def text(ctx, text):
            text = text.strip()
            if len(text) == 0:
                return
            edge = ctx.object(Edge)
            if edge.label:
                edge.label += text
            else:
                edge.label = text

    class Arrows:
        def begin(ctx, tag, attrs):
            edge = ctx.object(Edge)
            edge.sarrow = attrs["source"]
            edge.tarrow = attrs["target"]

    class Context(XmlWalker.Context):
        def __init__(self, walker):
            super().__init__(walker)
            self.nodes = {}
            self.edges = []
            self.entries = None

        def postprocess(self):
            # remove all comment nodes
            self.nodes = { k: v for k, v in self.nodes.items() if not v.isComment }
            # remove all edges that don't link functional nodes
            self.edges = [ x for x in self.edges if x.source in self.nodes and x.target in self.nodes ]

            for k, node in self.nodes.items():
                node.finish()

            # search all entry nodes
            self.entries = [ v for k, v in self.nodes.items() if v.isEntry ]

            for edge in self.edges:
                edge.snode = self.nodes[edge.source]
                edge.tnode = self.nodes[edge.target]
                edge.finish()

            # expand unnamed edges to unnamed nodes
            # strategy:
            # - create cross-product of all incoming edges with tag == None and all outgoing edges
            # - if node doesn't have at least one incoming edge with tag != None, erase the node
            erase = set()
            for k, node in self.nodes.items():
                nullin = set() # all incoming unnamed edges
                expanded = [] # all expanded edges
                has_nonnull = False # node has non-null incoming edges?
                has_incoming = len(node.incoming) > 0

                # collect all incoming tagless edges and remove them from the walker graph
                for it in node.incoming:
                    if it.tag:
                        has_nonnull = True
                    else:
                        nullin.add(it)
                        it.snode.outgoing.remove(it)
                node.incoming = [ x for x in node.incoming if not x in nullin ]
                self.edges = [ x for x in self.edges if not x in nullin ]

                # create cross-product of all collected edges with all outgoing edges
                for it in nullin:
                    for out in node.outgoing:
                        edge = Edge.make_expand(it, out)
                        expanded.append(edge)

                # insert the newly created edges into the walker graph
                for it in expanded:
                    it.snode.outgoing.append(it)
                    it.tnode.incoming.append(it)
                self.edges.extend(expanded)

                # delete node if it has only tagless incoming edges as can never be entered
                if has_incoming and not has_nonnull: # ensure that entry node isn't removed
                    erase.add(node)
                    for it in node.outgoing:
                        it.tnode.incoming.remove(it)
                    self.edges = [ x for x in self.edges if not x in node.outgoing ]
 
            # node can now be converted to a list, since graphml id lookups are no longer necessary
            self.nodes = [ v for k, v in self.nodes.items() if not v in erase ]
            self.nodes.sort(key = Node.key)
            for idx, x in enumerate(self.nodes):
                x.id = idx
                #print(x.id, x.name, x.mlid)
            self.byname = {}
            for x in self.nodes:
                if not x.name:
                    continue
                self.byname[x.name] = x.id


            self.edges.sort(key = Edge.key)
            self.descend = {}
            for x in self.edges:
                if x.tag == "*":
                    key = x.snode.id
                    if key in self.descend:
                        self.descend[key].append((None, x.tnode.id))
                    else:
                        self.descend[key] = [ (None, x.tnode.id) ]
                else:
                    key = (x.snode.id, x.tag)
                    self.descend[key] = x.tnode.id


        def validate(self):
            graph = set()
            for x in self.edges:
                assert x.tag # tag must not be empty as tagless edges have been expanded
                link = (x.snode, x.tnode, x.tag)
                assert not link in graph # the tuple (source, target, tag) must be unique
                graph.add(link)

            outgoing = set()
            incoming = set()
            for v in self.nodes:
                assert not v.isComment # comment nodes must have been removed
                for x in v.outgoing:
                    assert not x in outgoing # edge must not occur in multiple outgoing lists
                    assert x in self.edges # edge must be listed in self.edges
                    outgoing.add(x)
                for x in v.incoming:
                    assert not x in incoming # edge must not occur in multiple incoming lists
                    assert x in self.edges # edge must be listed in self.edges
                    incoming.add(x)

            for x in self.edges:
                assert x in outgoing # each edge must exist in one outgoing/incoming list
                assert x in incoming
                assert x.tnode in self.nodes # edge enpoints must be listed in self.nodes
                assert x.snode in self.nodes
                assert not x.tnode.isEntry # edge must not loop back to entry node

            assert len(self.entries) == 1 # there must be exactly one entry node

    walkerdef = GraphmlWalker(locals())
    ctx = walkerdef.saxwalk(filename, Context)
    ctx.postprocess()
    ctx.validate()

    class Walker(XmlWalker):
        descend = ctx.descend
        byname = ctx.byname
        nnode = len(ctx.nodes)

        def __init__(self, handlers, handlerPrefix = ""):
            self.byname = Walker.byname
            self.descend = Walker.descend
            super().__init__(Walker.nnode, handlers, handlerPrefix)

        def dump():
            print(Walker.byname)
            print(Walker.descend)

        def assertEqual(byname, descend):
            pass
            # assert(byname == Walker.byname)
            # assert(descend == Walker.descend)
            # TODO implement a true graph comparison algorithm, since the order for unnamed nodes is not defined

    return Walker

