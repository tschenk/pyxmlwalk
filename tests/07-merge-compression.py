# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("07.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'ChildA': 2, 'ChildB': 3, 'GrandChild': 4},
        {(0, 'root'): 1, (1, 'child-a'): 2, (1, 'child-b'): 3, (2, 'grand-child'): 4, (3, 'grand-child'): 4}
)

class ChildA:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag=", tag, " id=", attrs["id"], " depth=", ctx.depth(), sep = "")

class ChildB:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag=", tag, " id=", attrs["id"], " depth=", ctx.depth(), sep = "")

class GrandChild:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag=", tag, " id=", attrs["id"], " depth=", ctx.depth(), sep = "")

walker = MyWalker(locals())

walker.saxwalk("07.xml")

