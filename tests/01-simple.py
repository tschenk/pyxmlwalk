# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

# Load the .graphml-file and create a class from it that inherits xmlwalk.Walker
MyWalker = xmlwalk.make_walker_from_graphml("01.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'MyRoot': 1},
        {(0, 'root'): 1}
)

# Node handler classes:
# For each named node in the .graphml-file, you may create a sax-like handler class.
# The additional 'ctx' parameter is an instance of xmlwalk.Context.
# During the walk, it is always the same instance.
# It can be used to either store custom data across callbacks
# or to access the walk state.

# Note that this example uses static methods as callbacks.
# More on static callbacks vs. handler instances later.
class MyRoot:
    # Each time the MyRoot node is entered, this function called.
    # The parameters specify the xml element's tag name and its attributes
    # which caused the walker to enter the node.
    def begin(ctx, tag, attrs):
        print("begin tag")
        print("  tag name      :", tag)
        print("  tag attributes:", attrs)

    # Each time text within an element is encountered, this function is called.
    def text(ctx, text):
        print("text")
        print("  data          :", text)

    # When the current node entered in begin() is left, this function is called.
    # The tag name is given once more.
    def end(ctx, tag):
        print("end tag")
        print("  tag name      :", tag)

# Create a walker instance. The function parameter must be a dictionary
# which maps the node name in the .graphml-file to the desired handler class.
# Note that handlers are optional and don't have to be specified for a node
# if handling of a node is not desired.
# Here we use 'locals()' so that the locally defined classes are used as handlers.
walker = MyWalker(locals())

# Run the walker using a sax parser. This walks through the graph
# in the same order as the elements are specified in the .xml file.
# Particularly it doesn't sort child elements by name.
walker.saxwalk("01.xml")

