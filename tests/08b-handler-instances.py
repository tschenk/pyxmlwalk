# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("08.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'GenericNode': 5, 'Node': 6, 'NodeLabel': 7, 'Shape': 8},
        {(0, 'graphml'): 1, (1, 'graph'): 2, (2, 'node'): 6, (3, 'y:GenericNode'): 5, (3, 'y:ShapeNode'): 4, (4, 'y:NodeLabel'): 7, (4, 'y:Shape'): 8, (5, 'y:NodeLabel'): 7, (6, 'data'): 3}
)

# This example demonstrates, how data can be collected during a walk.
# To make this work, the handler classes can be instantiated.
# This simply requires renaming the begin() method to the constructor
# and adding a self parameter to all callback functions.

# This handler will be instantiated for every time Node is entered,
# i.e. instead of calling begin(), the constructor will be called.
# In this example, the Node object will be used to collect all the information of a node
# so that it is available when end() is called.
class Node:
    def __init__(self, ctx, tag, attrs):
        self.mydata = { "attrs": attrs, "label": [] }

    def end(self, ctx, tag):
        ctx.print("end node: ", self.mydata)

# The following handlers all are nested within the Node handler,
# since it is specified this way in the .graphml-file.
# Therefore, there is an active Node object for all callbacks to these
# handlers. This object can be retrieved from the context using the
# ctx.object(<class>) method.

class GenericNode:
    def begin(ctx, tag, attrs):
        node = ctx.object(Node)
        node.mydata["generic_node"] = attrs["configuration"]

class Shape:
    def begin(ctx, tag, attrs):
        node = ctx.object(Node)
        node.mydata["shape"] = attrs["type"]

class NodeLabel:
    def text(ctx, text):
        text = text.strip()
        if text:
            node = ctx.object(Node)
            node.mydata["label"].append(text)

walker = MyWalker(locals())

walker.saxwalk("08.graphml")

