# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("08.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'GenericNode': 5, 'Node': 6, 'NodeLabel': 7, 'Shape': 8},
        {(0, 'graphml'): 1, (1, 'graph'): 2, (2, 'node'): 6, (3, 'y:GenericNode'): 5, (3, 'y:ShapeNode'): 4, (4, 'y:NodeLabel'): 7, (4, 'y:Shape'): 8, (5, 'y:NodeLabel'): 7, (6, 'data'): 3}
)

# Get some information from the walker definition file itself:
# node ids, node shapes and node labels
class Node:
    def begin(ctx, tag, attrs):
        ctx.print("begin node: ", attrs["id"], sep = "")

    def end(ctx, tag):
        ctx.print("end node")

# The comment node is a GenericNode which has its type defined
# by the 'configuration' attribute.
class GenericNode:
    def begin(ctx, tag, attrs):
        ctx.print("generic node:", attrs)

# All functional nodes are Shape nodes, which in addition to being a ShapeNode
# they also have a Shape. The 'type' attribute specifies, which
# shape the node has.
class Shape:
    def begin(ctx, tag, attrs):
        ctx.print("node shape:", attrs)

# Both comment nodes and functional nodes have a NodeLabel,
# its text is the text written inside the node.
class NodeLabel:
    def text(ctx, text):
        text = text.strip()
        if text:
            ctx.print("label='", text, "'", sep = "")

walker = MyWalker(locals())

walker.saxwalk("08.graphml")

