# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("06.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'ChildNode': 2},
        {(0, 'root'): 1, (1, 'child'): 2}
)

# The ChildNode handler is the only handler, MyRoot is now
# an anonymous node and therefore cannot be handled.
class ChildNode:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag=", tag, " id=", attrs["id"], " depth=", ctx.depth(), sep = "")

walker = MyWalker(locals())

walker.saxwalk("06.xml")

