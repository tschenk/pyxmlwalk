# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("03.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'ChildNode': 1, 'MyRoot': 2},
        {(0, 'root'): 2, (1, 'child'): 1, (2, 'child'): 1}
)

# Reduce output in order to make the structure more visible how the walker walks the file.
class MyRoot:
    def begin(ctx, tag, attrs):
        ctx.print("begin", tag)

    def end(ctx, tag):
        ctx.print("end", tag)

# The xml file structure defined in the .graphml-file determines,
# that this handler is used on <child> elements of <root> and on <child> elements of <child>,
# therefore it is called between MyRoot.begin() and MyRoot.end()
# and recursively for each child nesting between ChildNode.begin() and ChildNode.end().
class ChildNode:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag=", tag, " id=", attrs["id"], " depth=", ctx.depth(), sep = "")

    def end(ctx, tag):
        ctx.print("end   tag=", tag)

walker = MyWalker(locals())
walker.saxwalk("03.xml")


