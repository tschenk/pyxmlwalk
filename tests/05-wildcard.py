# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("05.graphml")
MyWalker.dump();

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'ChildNode': 2},
        {(0, 'root'): 1, (1, 'child'): 2}
)

# The ChildNode handler handles <child-a> elements only
class ChildNode:
    def begin(ctx, tag, attrs):
        ctx.print("[ChildNode] begin tag=", tag, " attrs=", attrs, sep = "")

# The CommonSaxNode acts like a common sax parser for all elements other than <child-a>
class CommonSaxNode:
    def begin(ctx, tag, attrs):
        ctx.print("[CommonSaxNode] begin tag=", tag, " attrs=", attrs, sep = "")

walker = MyWalker(locals())

walker.saxwalk("05.xml")

