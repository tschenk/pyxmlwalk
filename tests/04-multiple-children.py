# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("04.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'ChildNode': 1, 'MyRoot': 2},
        {(0, 'root'): 2, (2, 'child-a'): 1, (2, 'child-b'): 1}
)

class MyRoot:
    def begin(ctx, tag, attrs):
        ctx.print("begin", tag)

    def end(ctx, tag):
        ctx.print("end", tag)

# The ChildNode handler is now used by children with different tags.
class ChildNode:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag=", tag, " id=", attrs["id"], " depth=", ctx.depth(), sep = "")

walker = MyWalker(locals())

print("--- saxwalk():")
walker.saxwalk("04.xml")

# TODO implement domwalk
#print()
#print("--- domwalk():")
#walker.domwalk("04.xml")

