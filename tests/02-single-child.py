# Sanely import the single-file library without the python packages cruft
import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "../xmlwalk.py").load_module()

MyWalker = xmlwalk.make_walker_from_graphml("02.graphml")

# Only used by test: assert expected result
MyWalker.assertEqual(
        {'ChildNode': 1, 'MyRoot': 2},
        {(0, 'root'): 2, (2, 'child'): 1}
)

# Like in the previous example. Just don't print text, since
# in this example only whitespaces are used.
# Also use ctx.print(), which is similar to print() but also
# indents by the current walk depth.
# Additionally ctx.depth() is printed which indicates how deep the walker
# is within the xml structure.
class MyRoot:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag (MyRoot)")
        ctx.print("  walk depth    :", ctx.depth())
        ctx.print("  tag name      :", tag)
        ctx.print("  tag attributes:", attrs)

    def end(ctx, tag):
        ctx.print("end tag (MyRoot)")
        ctx.print("  walk depth    :", ctx.depth())
        ctx.print("  tag name      :", tag)

# This handler works exactly like the handler for MyRoot.
# The xml file structure defined in the .graphml-file determines,
# that this handler is used on <child> elements of <root>, therefore
# it is called between MyRoot.begin() and MyRoot.end().
class ChildNode:
    def begin(ctx, tag, attrs):
        ctx.print("begin tag (ChildNode)")
        ctx.print("  walk depth    :", ctx.depth())
        ctx.print("  tag name      :", tag)
        ctx.print("  tag attributes:", attrs)

    def end(ctx, tag):
        ctx.print("end tag (ChildNode)")
        ctx.print("  walk depth    :", ctx.depth())
        ctx.print("  tag name      :", tag)

walker = MyWalker(locals())
walker.saxwalk("02.xml")

