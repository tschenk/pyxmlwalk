import importlib.machinery
xmlwalk = importlib.machinery.SourceFileLoader("xmlwalk", "xmlwalk.py").load_module()

GraphmlWalker = xmlwalk.make_walker_from_graphml("bootstrap.graphml")

GraphmlWalker.dump()

